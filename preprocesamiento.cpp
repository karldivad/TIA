#include <iostream>
#include "CImg.h"
#include <tuple>

using namespace cimg_library;
using namespace std;

tuple<string,string> getExtension(string file){
	string pre = "";
	string post = "";
	bool flag = 0;
	for(char c : file){
		if(c == '.') flag = 1;
		else if(flag == 0) pre.push_back(c);
		else post.push_back(c);
	}
	return make_tuple(pre,post);
}

string convertToBMP(string file){
	string pre;
	string post;
	string command = "";
	tie(pre,post) = getExtension(file);
	if(post != "bmp"){
		command = "convert " + file + " " + pre + ".bmp";
		system(command.c_str());
		return pre + ".bmp";
	}
	return file;
}

CImg<float> binary(CImg<float> &img){
	CImg<float> res(img.width(),img.height(),1,1);
	float Y = 0;
	float Cr = 0;
	float Cb = 0;
	float pixel = 0;
	float L = 0;
	float a = 0;
	float b = 0;
	cout<<img.height()<<endl;
	cout<<img.width()<<endl;
	for(int i = 0; i < img.width(); i++){
		for(int j = 0; j < img.height(); j++){
			/*
			Y = img(i,j,0);
			Cb = img(i,j,1);
			Cr = img(i,j,2);
			*/
//			pixel = img(i,j,0);
			/*

		

			if(Y > 80 and 85 < Cb and Cb < 135 and 125 < Cr and Cr < 180){
				res(i,j,0) = 255;
			}
			else{
				res(i,j,0) = 0;
			}
			*/

			L = img(i,j,0);
			a = img(i,j,1);
			b = img(i,j,2);

			
			/*
			if(76 <= Cb and Cb <= 127 and 132 <= Cr and Cr <= 173){
				res(i,j,0) = 255;
			}
			else{
				res(i,j,0) = 0;
			}
			*/

//			if(80 <= Cb and Cb <= 120 and 133 <= Cr and Cr <= 173){
//				res(i,j,0) = 255;
//			}
//			if(pixel < 80){
//				res(i,j,0) = 255;
//			}
			
			
			//if(L >= 40 and L <= 85 and a >= 5 and a <= 55 and b >= -7 and b <= 25){
			//	res(i,j,0) = 255;
			//}

			/*if(L >= 17 and L <= 90 and a >= 26 and a <= 85 and b >= -30 and b <= 15){
				res(i,j,0) = 255;
			}*/
			if(L >= 17 and L <= 90 and a >= 26 and a <= 85 and b >= -30 and b <= 15){
				res(i,j,0) = 255;
			}
			else{
				res(i,j,0) = 0;
			}				
		}
	}
	return res;
}

CImg<float> gray(CImg<float> &img){
	/*CImg<float> res(img);
	for(int i = 0; i < res.height(); i++){
		for(int j = 0; j < res.width(); j++){
			res(j,i,0) = img(j,i,0) * 0.21;
			res(j,i,1) = img(j,i,1) * 0.72;
			res(j,i,2) = img(j,i,2) * 0.07;
		}
	}
	return res;
	*/
	return img.get_channel(0);
}

int main(int argv, char ** argc){
	if(argv != 2){
		cout<<"Faltan Argumentos <Img>"<<endl;
		return 0;
	}
	string file(argc[1]);

	string fileImgBmp = convertToBMP(file);
	
	/*
	CImg<float> A(file.c_str());
	CImg<float> B = A.get_RGBtoYCbCr();
	CImg<float> C = binary(B);
	(A,B,C).display();
	C.save("out.bmp");
	*/
	

	CImg<float> A(fileImgBmp.c_str());
	CImg<float> B = A.get_RGBtoLab();
	//CImg<float> C = gray(B);
	CImg<float> E = binary(B);
	E.save("out.bmp");

//	(A,B,E).display();
	
}