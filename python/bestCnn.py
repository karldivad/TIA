import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.utils import np_utils
from keras.datasets import mnist
from matplotlib import pyplot as plt
from keras import backend as K
import ImgOp as Io
from keras.models import load_model


np.random.seed(123)

FILE_NAMES_TRAIN = "namesTrain.dat"
FILE_NAMES_TEST = "namesTest.dat"
FILE_CLASS_TRAIN = "clasesTrain.dat"
FILE_CLASS_TEST = "clasesTest.dat"
DIR_TRAIN = "skin_train_50"
DIR_TEST = "skin_test_50"

HEIGHT_IMGS = 50
WEIGHT_IMGS = 50	
N_TRAIN = 551
N_TEST = 348
N_CLASES = 27

BATCH_SIZE = 64
EPOCHS = 120


X_train = Io.dirToArray(DIR_TRAIN,FILE_NAMES_TRAIN,N_TRAIN,HEIGHT_IMGS,WEIGHT_IMGS)
X_test = Io.dirToArray(DIR_TEST,FILE_NAMES_TEST,N_TEST,HEIGHT_IMGS,WEIGHT_IMGS)


X_train = Io.binarize(X_train)
X_test = Io.binarize(X_test)


Y_train = Io.getClassFromFile(FILE_CLASS_TRAIN)
Y_test = Io.getClassFromFile(FILE_CLASS_TEST)

#(X2_train, y2_train), (X2_test, y2_test) = mnist.load_data()

X_train = X_train.reshape(X_train.shape[0],HEIGHT_IMGS, WEIGHT_IMGS,1)
X_test = X_test.reshape(X_test.shape[0],HEIGHT_IMGS, WEIGHT_IMGS,1)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

Y_train = np_utils.to_categorical(Y_train, N_CLASES)
Y_test = np_utils.to_categorical(Y_test, N_CLASES)

model = Sequential()

#model.add(Conv2D(128, (2,2), activation='relu', input_shape=(HEIGHT_IMGS,WEIGHT_IMGS,1)))
#model.add(Conv2D(128, (2,2), activation='relu'))
#model.add(MaxPooling2D(pool_size=(2,2)))
#model.add(Conv2D(128, (3,3), activation='relu'))
#model.add(Conv2D(128, (3,3), activation='relu'))
#model.add(MaxPooling2D(pool_size=(2,2)))
#model.add(Conv2D(128, (4,4), activation='relu'))
#model.add(Conv2D(128, (4,4), activation='relu'))
#model.add(MaxPooling2D(pool_size=(2,2)))
#model.add(Dropout(0.25))
#model.add(Flatten())
#model.add(Dense(128, activation='relu'))
#model.add(Dropout(0.25))
#model.add(Dense(N_CLASES, activation='softmax'))

#model.add(Conv2D(128, (5,5), padding='same', input_shape=(HEIGHT_IMGS,WEIGHT_IMGS,1)))
model.add(Conv2D(32, (10,10), padding='same', input_shape=X_train.shape[1:]))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(32, (10,10), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))
model.add(Conv2D(64, (10,10), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(64, (10,10),padding = 'same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))
model.add(Conv2D(32, (3,3),padding = 'same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(32, (3,3), padding = 'same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))
model.add(Flatten())
model.add(Dense(256))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Dropout(0.4))
model.add(Dense(128))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Dropout(0.4))
model.add(Dense(N_CLASES, activation='softmax'))




model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print (model.summary())
model.fit(X_train, Y_train, batch_size=BATCH_SIZE, epochs=EPOCHS, verbose=1)
score = model.evaluate(X_test, Y_test, verbose=1)	
pre = model.predict(X_test[:1], batch_size= 1, verbose = 1)
print (score)
print (pre)
model.save('modelcuack.h5')
print ("Saved")
