import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from matplotlib import pyplot as plt
from keras import backend as K
import ImgOp3 as Io
import sys
from keras.models import load_model

if(len(sys.argv) < 3):
	print ("Faltan Argumentos <Img> <Model>")
else:

	classMap = "/home/carlos/TIA/python/classMap"

	HEIGHT_IMGS = 50
	WEIGHT_IMGS = 50
	N_CLASES = 27

	fileImg = sys.argv[1]
	fileModel = sys.argv[2]

	np.random.seed(123)

	HEIGHT_IMGS = 50
	WEIGHT_IMGS = 50


	Io.resize(fileImg, HEIGHT_IMGS, WEIGHT_IMGS)


	test = Io.imgToArray(fileImg)
	Z_test = np.ndarray(shape = (1,test.shape[0],test.shape[1]), dtype=int)
	Z_test[0] = test
	

	Z_test = Io.binarize(Z_test)

	#plt.imshow(X_test[0], cmap=plt.get_cmap('gray'))
	#plt.show()

	Z_test = Z_test.reshape(Z_test.shape[0],HEIGHT_IMGS, WEIGHT_IMGS,1)

	Z_test = Z_test.astype('float32')
	Z_test /= 255
	

	

	#print ("Model" + fileModel)

	model = load_model(fileModel)

	#print ("End of Load")

	pre = model.predict(Z_test, batch_size= 1, verbose = 0)
	#print (pre)
	letra = Io.getClass(classMap,pre)
	print (letra)
	archivo = open("out.res",'w')
	archivo.write(letra)
	archivo.close()


