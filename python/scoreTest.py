import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from matplotlib import pyplot as plt
from keras import backend as K
from keras.models import load_model
import ImgOp as Io

np.random.seed(123)

FILE_NAMES_TRAIN = "namesTrain.dat"
FILE_NAMES_TEST = "namesTest.dat"
FILE_CLASS_TRAIN = "clasesTrain.dat"
FILE_CLASS_TEST = "clasesTest.dat"
DIR_TRAIN = "skin_train_50"
DIR_TEST = "skin_test_50"

HEIGHT_IMGS = 50
WEIGHT_IMGS = 50	
N_TRAIN = 551
N_TEST = 348
N_CLASES = 27

BATCH_SIZE = 64
EPOCHS = 120


X_train = Io.dirToArray(DIR_TRAIN,FILE_NAMES_TRAIN,N_TRAIN,HEIGHT_IMGS,WEIGHT_IMGS)
X_test = Io.dirToArray(DIR_TEST,FILE_NAMES_TEST,N_TEST,HEIGHT_IMGS,WEIGHT_IMGS)


X_train = Io.binarize(X_train)
X_test = Io.binarize(X_test)


Y_train = Io.getClassFromFile(FILE_CLASS_TRAIN)
Y_test = Io.getClassFromFile(FILE_CLASS_TEST)

#(X2_train, y2_train), (X2_test, y2_test) = mnist.load_data()

X_train = X_train.reshape(X_train.shape[0],HEIGHT_IMGS, WEIGHT_IMGS,1)
X_test = X_test.reshape(X_test.shape[0],HEIGHT_IMGS, WEIGHT_IMGS,1)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

Y_train = np_utils.to_categorical(Y_train, N_CLASES)
Y_test = np_utils.to_categorical(Y_test, N_CLASES)

model = load_model('mejormodelo.h5')

#score = model.evaluate(X_test, y_test, verbose=1) #[loss,acuracy]
#print y_test[0]
#print y_test[1]
#pre = model.predict(X_test[:2], batch_size= 1, verbose = 1)
#print pre

score = model.evaluate(X_test, Y_test, verbose=1)
print("%s: %.2f%%" % (model.metrics_names[1], score[1]*100))
print("%s: %.2f%%" % (model.metrics_names[0], score[0]*100))

print score

